-- Copyright (c) 2009, r. brian harrison.  All rights reserved.

--[[

Display need/greed/pass choices made by other party/raid members in
the tooltips of my need/greed/pass buttons.

The tooltip will be updated if another party/raid member rolls while
the tooltip is being displayed.

This should also work in the unusual circumstances when:

* a boss drops more than NUM_GROUP_LOOT_FRAMES items, and others have
  rolled on items before player has shown that item.

* a boss drops multiple of the same item (assuming their links are in
  fact identical).

* players have joined or left the party/raid during between active frame.

--]]

-- XXX localization needed
local TOTAL_FMT = "%d rolled %s"
local NO_RESPONSE_FMT = "%d not responded"

local tooltip_on_enter

local rolls = {
	["need"] = { },
	["greed"] = { },
	["pass"] = { },
}
local no_response = { }

-- for debugging
GroupLootMonitor_rolls = rolls
GroupLootMonitor_no_response = no_response

local function get_party_or_raid_members(members)
	if members then
		for k in pairs(members) do
			members[k] = nil
		end
	else
		members = { }
	end
	if GetNumRaidMembers() > 0 then
		for i = 1, MAX_RAID_MEMBERS do
			table.insert(members, (GetRaidRosterInfo(i)))
		end
	else
		members[1] = UnitName("player")
		for i = 1, MAX_PARTY_MEMBERS do
			table.insert(members, (UnitName("party"..i)))
		end
	end
	return members
end

local function table_remove_value(t, remove_v)
	-- t is an array; renumber indexes of contiguous subsequent elements
	for i, v in ipairs(t) do
		if v == remove_v then
			table.remove(t, i)
		end
	end
	-- t is a dictionary table or sparse array
	for k, v in pairs(t) do
		if v == remove_v then
			t[k] = nil
		end
	end
	return t
end

local function get_no_response(link)
	local no_resp = no_response[link]
	if not no_resp then
		no_resp = get_party_or_raid_members()
		table.sort(no_resp)
		no_response[link] = no_resp
	end
	return no_resp
end

local function rolled(roll, link, name)
	local rollers = rolls[roll][link]
	if not rollers then
		rollers = { }
		rolls[roll][link] = rollers
	end
	table.insert(rollers, name)
	table_remove_value(get_no_response(link), name, true)
	if tooltip_on_enter then
		tooltip_on_enter()
	end
end

local function need(...)
	rolled("need", ...)
end

local function greed(...)
	rolled("greed", ...)
end

local function pass(...)
	rolled("pass", ...)
end

local function done(link)
	rolls["need"][link] = nil
	rolls["greed"][link] = nil
	rolls["pass"][link] = nil
	no_response[link] = nil
end

local function format_to_pattern(s)
	return "^"..string.gsub(s, "%%s", "(.+)").."$"
end

local loot_patterns = {
	[format_to_pattern(LOOT_ROLL_NEED)] = need,
	[format_to_pattern(LOOT_ROLL_GREED)] = greed,
	[format_to_pattern(LOOT_ROLL_PASSED)] = pass,
	[format_to_pattern(LOOT_ROLL_PASSED_AUTO)] = pass,
	[format_to_pattern(LOOT_ROLL_PASSED_AUTO_FEMALE)] = pass,
	[format_to_pattern(LOOT_ROLL_ALL_PASSED)] = done,
	[format_to_pattern(LOOT_ROLL_WON)] = done,
}

local roll_buttons = {
	["need"] = "RollButton",
	["greed"] = "GreedButton",
	["pass"] = "PassButton",
}

for i = 1, NUM_GROUP_LOOT_FRAMES do
	for roll, button_suffix in pairs(roll_buttons) do
	local button = getglobal("GroupLootFrame"..i..button_suffix)
	local old_on_enter = button:GetScript("OnEnter")
	local old_on_leave = button:GetScript("OnLeave")
	local fn
	fn = function()
			 old_on_enter(button)
			 tooltip_on_enter = fn
			 local link = GetLootRollItemLink(button:GetParent().rollID)
			 local rollers = rolls[roll][link]
			 if rollers and #rollers > 0 then
				 table.sort(rollers)
				 GameTooltip:AddLine("")
				 local msg = string.format("|cff404040"..TOTAL_FMT.."|r",
										   #rollers, roll)
				 GameTooltip:AddLine(msg)
				 for _, roller in ipairs(rollers) do
					 GameTooltip:AddLine(roller)
				 end
			 end
			 local no_resp = get_no_response(link)
			 if no_resp and #no_resp > 0 then
				 GameTooltip:AddLine("")
				 local msg = string.format("|cff404040"..NO_RESPONSE_FMT.."|r", #no_resp)
				 GameTooltip:AddLine(msg)
				 for _, roller in ipairs(no_resp) do
					 GameTooltip:AddLine("|cffff8080"..roller.."|r")
				 end
			 end
			 GameTooltip:Show()
		 end
	button:SetScript("OnEnter", fn)
	button:SetScript("OnLeave",
					 function(self)
						 old_on_leave(self)
						 tooltip_on_enter = nil
					 end)
	end
end

local frame = CreateFrame("Frame", "GroupLootMonitor")
frame:SetScript("OnEvent",
				function(self, event, message)
					for pattern, fn in pairs(loot_patterns) do
						local _, _, name, link = string.find(message, pattern)
						if name and link then
							fn(link, name)
							break
						end
					end
				end)
frame:RegisterEvent("CHAT_MSG_LOOT")
